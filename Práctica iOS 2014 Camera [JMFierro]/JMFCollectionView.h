//
//  JMFCollectionView.h
//  Práctica iOS 2014 Camera [JMFierro]
//
//  Created by José Manuel Fierro Conchouso on 05/04/14.
//  Copyright (c) 2014 José Manuel Fierro Conchouso. All rights reserved.
//

/*
 Este código de collectionView con celdas peronalizadas
 esta basado en el codigo de Brandon Trebitowski:
 http://www.raywenderlich.com/22324/beginning-uicollectionview-in-ios-6-part-12
 */



#import <UIKit/UIKit.h>
#import "ImageFlickr.h"
#import "JMFModel.h"
#import "JMFCameraViewController.h"




@interface JMFCollectionView : UIViewController <UICollectionViewDelegateFlowLayout, UICollectionViewDataSource, CameraViewControllerDelegate, UITextFieldDelegate>

@property (nonatomic, strong) JMFModel *model;

@property (nonatomic, weak) IBOutlet UITextField *searchTextField;
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *activiyIndicator;

- (IBAction)btnTakePhoto:(id)sender;
- (IBAction)btnGelery:(id)sender;


- (IBAction)clickBackground:(id)sender;


@end
