//
//  JMFTableImageViewController.h
//  Práctica iOS 2014 Camera [JMFierro]
//
//  Created by José Manuel Fierro Conchouso on 08/04/14.
//  Copyright (c) 2014 José Manuel Fierro Conchouso. All rights reserved.
//

/*
 Este código de TableView de celdas peronalizadas
 esta basado en el codigo de Tibolte/TGFoursquareLocationDetail-Demo:
 https://github.com/Tibolte/TGFoursquareLocationDetail-Demo
 */



@import CoreLocation;

#import <UIKit/UIKit.h>
#import <ImageIO/ImageIO.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>

#import "JMFModel.h"
#import "JMFImage.h"
#import "ImageFlickr.h"

#define kJMFTableImageViewControlleFacesRects @"kJMFTableImageViewControlleFacesRects"
#define kJMFTableImageViewControlleRemove @"kJMFTableImageViewControlleRemove"


@interface JMFImageTableViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, CLLocationManagerDelegate, MKMapViewDelegate>


//@property (nonatomic, strong) JMFMetaData *metaData;
//@property (nonatomic,strong) JMFImage *imageCamera;
//@property (nonatomic, strong) ImageFlickr *imageFlickr;
//@property (nonatomic, strong) UIImage *image, *imageThumbnail;


-(id) initWithImage:(JMFImage *) image;
-(id) initWithImageCamera:(JMFImage *) imageCamera;
-(id) initWithFlickrPhoto:(ImageFlickr *)flickrPhoto;


@end
